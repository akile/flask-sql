from flask import Flask, render_template, request, json, jsonify
from flask_cors import CORS
import sqlalchemy as db
import os

application = Flask(__name__)

engine = db.create_engine("oracle+cx_oracle://logbook_ti_dev:"+os.environ['databasepassword']+"@devdb11-s.cern.ch:10121/DEVDB112")
metadata = db.MetaData()
census = db.Table('USER_TEST', metadata, autoload=True, autoload_with=engine)
conn = engine.connect()


# Enables CORS
CORS(application)

application.config['SECRET_KEY'] = os.urandom(16)


user = db.table('USER_TEST',
        db.column('ID'),
        db.column('USERNAME'),
        db.column('EMAIL'),
)


@application.route('/users', methods=['GET', 'POST'])
def users():
    if request.method == 'GET':
        conn = engine.connect()
        query = db.select([census])
        ResultProxy = conn.execute(query)
        ResultSet = ResultProxy.fetchall()

        # Prints out everything, can not see belonging column from database
        print(ResultSet)
        return 'GET METHOD'
    elif request.method == 'POST':
        req = json.loads(request.data)
        conn = engine.connect()
        ins = user.insert().values(USERNAME=req['username'], EMAIL=req['email'])
        result = conn.execute(ins)
        conn.close()
        return ''

@application.route('/users/<int:id>', methods=['GET', 'PUT', 'DELETE'])
def User(id):
    if request.method == 'GET':
        conn = engine.connect()
        ins = db.select([user], user.c.ID == id)
        result = conn.execute(ins)
        ResultProxy = result.fetchone()
        conn.close()
        return ''
    elif request.method == 'PUT':
        return ''

    elif request.method == 'DELETE':
        return ''
    else :
        return ''

if __name__ == '__main__':
    # from views import *
    application.run('localhost', 5000, debug=True)
