# FROM centos:7
FROM gitlab-registry.cern.ch/linuxsupport/cc7-base

ENV PYTHON_VERSION "3.6.0"

RUN yum install -y \
    wget \
    gcc make \
    bzip2-devel zlib-devel epel-release python-pip libaio

RUN wget https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz \
    && tar xvf Python-${PYTHON_VERSION}.tgz \
    && cd Python-${PYTHON_VERSION} \
    && ./configure --prefix=/usr/local \
    && make \
    && make altinstall \
    && cd / \
    && rm -rf Python-${PYTHON_VERSION}*

ENV PATH "/usr/local/bin:${PATH}"

RUN pip install --upgrade pip

# Install the requirements for running the application
COPY requirements.txt /tmp/requirements.txt
RUN pip install --trusted-host pypi.python.org -r /tmp/requirements.txt

# install dev tools 
# RUN yum-config-manager --enable rhel-server-rhscl-7-rpms && \
#     yum-config-manager --enable rhel-7-server-rpms && \
# 	yum-config-manager --enable rhel-7-server-eus-rpms && \
#     yum-config-manager --enable rhel-7-server-optional-rpms && \	
#     yum -y groupinstall 'Development Tools' && \
#     yum clean all

# RUN yum-config-manager --enable rhel-server-rhscl-7-rpms && \
#     yum-config-manager --enable rhel-7-server-rpms && \
# 	yum-config-manager --enable rhel-7-server-eus-rpms && \
#     yum-config-manager --enable rhel-7-server-optional-rpms && \	
# 	INSTALL_PKGS="wget libaio-devel" && \
#     yum -y --setopt=tsflags=nodocs install $INSTALL_PKGS && \
#     rpm -V $INSTALL_PKGS && \
#     yum clean all


# Copy and install the Oracle dependencies.
COPY instantclient/* /tmp/

RUN cd /tmp/ && rpm -Uvh /tmp/oracle-instantclient*.rpm
# Change back to regular user
ENV ORACLE_HOME /usr/lib/oracle/18.3/client64/lib

# Sets the environmental variable LD_LIBRARY_PATH to the appropriate directory for the Instant Client Version    
ENV LD_LIBRARY_PATH /usr/lib/oracle/18.3/client64/lib:${LD_LIBRARY_PATH}

# USER 1001
