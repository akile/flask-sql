from wsgi import db

class User(db.Model):
    __tablename__ = 'FLASK_TEST'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)

    def __init__(self,username, email, id):
        self.username = username
        self.email = email
        self.id = id


def create_new_user(new_username, new_email, new_id):
    new_user = User(new_username, new_email, new_id)
    db.session.add(new_user)
    db.session.commit()
    return new_user
