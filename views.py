from flask import render_template, request
from wsgi import application
from models import User, create_new_user

@application.route('/api')
def index():
    users = User.query.all()
    return render_template('index.html', users=users)


@application.route('/api/add', methods=['GET','POST'])
def add():
    if request.method == 'GET':
        return render_template('add.html')
    
    user_username = request.form.get('username_field')
    user_email = request.form.get('email_field')
    user_id = request.form.get('id_field')

    user = create_new_user(user_username, user_email, user_id)
    return render_template('add.html', user=user)